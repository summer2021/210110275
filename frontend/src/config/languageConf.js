export default {
  c: {
    displayName: 'C',
    mimeType: 'text/x-csrc',
    hljsLanguage: 'c',
    color: 'green',
    multiFile: false
  },
  cpp: {
    displayName: 'C++',
    mimeType: 'text/x-c++src',
    hljsLanguage: 'cpp',
    color: 'cyan',
    multiFile: false
  },
  java: {
    displayName: 'Java',
    mimeType: 'text/x-java',
    hljsLanguage: 'java',
    color: 'red',
    multiFile: false
  },
  python3: {
    displayName: 'Python3',
    mimeType: 'text/x-python',
    hljsLanguage: 'python',
    color: 'orange',
    multiFile: false
  },
  c_multifile: {
    displayName: 'C（多文件）',
    mimeType: 'text/x-csrc',
    hljsLanguage: 'c',
    color: 'green',
    multiFile: true
  }
}
